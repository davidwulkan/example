export interface Customer {
    id:string,
    name:string,
    income:number,
    years:number,
}
