import { ImageService } from './../image.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  selectedNetwork;
  invalid: string;
  show: boolean = false;
  networks: string[]  = ['BBC', 'NBC','CNN' ];
  text:string;
  category:string = 'No category';
  categoryImage:string;
  
  constructor(private route:ActivatedRoute, private classifyService:ClassifyService, private ImageService:ImageService) { }

  classify(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.ImageService.images[res];
      }
    )
  }

  ngOnInit(): void {
    this.selectedNetwork = this.route.snapshot.params.network;
    
    if (this.selectedNetwork.toUpperCase()!='BBC' && this.selectedNetwork.toUpperCase()!='CNN' && this.selectedNetwork.toUpperCase()!='NBC' ) {
      this.show = true
      this.invalid=this.selectedNetwork;
    }
    else{
      this.selectedNetwork=this.selectedNetwork.toUpperCase();
    }

  }

}

