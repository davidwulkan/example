import { Customer } from './../interfaces/customer';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;
  @Input() income:number;
  @Input() years:number;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  onSubmit(){
    
  }


  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, income:this.income, years:this.years};
    this.update.emit(customer);
    if(this.formType == "Add Customer"){
      this.name = null;
      this.income = null;
      this.years = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
