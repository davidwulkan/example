import { AuthService } from './../auth.service';
import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersA } from '../interfaces/users-a';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  
  constructor(private UsersService:UsersService, public AuthService:AuthService, private router:Router) { }

  users$:Observable<UsersA>
  errorMessage:string;
  isError: boolean = false;
  password:string;
  email:string;

  register(email:string, password:string){
    this.AuthService.register(email,password).then(res => {
      console.log("registered");
      console.log(email,password);
      window.alert("Successful registration!");
      this.router.navigate(['/welcome']);
    }).catch(
      err => {
        console.log(err);
        this.isError = true;
        window.alert(err.message);
        this.errorMessage = "Try Again";        
      }
    )
  }

  ngOnInit(): void {
    this.users$ = this.UsersService.getUsers(); 
  }


}