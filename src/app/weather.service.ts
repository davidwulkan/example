import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { throwError, Observable } from 'rxjs';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "783274be10c0b4e57ed4b9115c85d83b";
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{ //api call function
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){ //handling error
    console.log(res.error);
    return throwError(res.error || 'Server error');
  }

  private transformWeatherData(data:WeatherRaw):Weather{ //transform weather raw to weather
    return{
      name:data.name, //name shel weather yhie data.name shel weatherRaw
      country:data.sys.country,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`, //hamarat icon le kishur
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
}
