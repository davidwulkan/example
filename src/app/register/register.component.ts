import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string;
  isError: boolean = false;

  constructor(private auth:AuthService, private router:Router) { }

  onSubmit(){
    this.auth.register(this.email,this.password).then(res => {
      console.log("registered");
      window.alert("Successful registration!");
      this.router.navigate(['/welcome']);
    }).catch(
      err => {
        console.log(err);
        this.isError = true;
        window.alert(err.message);
        this.errorMessage = "Try Again";        
      }
    )
  }

  ngOnInit(): void {
  }

}