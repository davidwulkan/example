import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = " https://umlfcr3g89.execute-api.us-east-1.amazonaws.com/exampletest";

  prediction(income:number, years:number){
    let json = {
      "data": 
        {
          "income": income,
          "years": years
        }
    }
    let body  = JSON.stringify(json);
    console.log("testing");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }
  constructor(private http:HttpClient) { }
}
