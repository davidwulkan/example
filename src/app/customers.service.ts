import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

customersCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection('users');

public getCustomers(userId){
  this.customersCollection = this.db.collection(`users/${userId}/customers`,
  ref => ref.orderBy('name', 'asc').limit(4));
  return this.customersCollection.snapshotChanges()
}
  
nextPage(userId,startAfter): Observable<any[]>{
  this.customersCollection = this.db.collection(`users/${userId}/customers`, 
  ref => ref.limit(4).orderBy('name', 'asc').startAfter(startAfter))    
  return this.customersCollection.snapshotChanges();
}
    
prevPage(userId,startAt): Observable<any[]>{
  this.customersCollection = this.db.collection(`users/${userId}/customers`, 
  ref => ref.limit(4).orderBy('name', 'asc').startAt(startAt))    
  return this.customersCollection.snapshotChanges();
}
    

deleteCustomer(Userid:string, id:string){
  this.db.doc(`users/${Userid}/customers/${id}`).delete();
}

addCustomer(userId:string, name:string, income:number, years:number){
  const customer = {name:name, income:income, years:years};
  this.userCollection.doc(userId).collection('customers').add(customer);
}

updateCustomer(userId:string, id:string, name:string, income:number, years:number){
  this.db.doc(`users/${userId}/customers/${id}`).update(
  {
    name:name,
    income:income,
    years:years
  }
 );
}

public addPredict(userId:string,id:string,prediction:string){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      prediction:prediction
    }
  )
}


  
    

constructor(private db:AngularFirestore) { }

}
