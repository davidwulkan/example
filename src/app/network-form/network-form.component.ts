import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-network-form',
  templateUrl: './network-form.component.html',
  styleUrls: ['./network-form.component.css']
})
export class NetworkFormComponent implements OnInit {

  networks:Object[] = [{id:1, name:'BBC'},{id:2, name:'NBC'},{id:3, name:'CNN'}]
  network:string;  
  

  onSubmit(){
    this.router.navigate(['/classify',this.network]);
  }

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

}
