import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersA } from './interfaces/users-a';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private URL = "https://jsonplaceholder.typicode.com/users/";
  constructor(private http: HttpClient) { }


  
  getUsers():Observable<UsersA>{
    return this.http.get<UsersA>(this.URL); 
  }

  
}