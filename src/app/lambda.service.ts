import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LambdaService {

  private url = "https://qee2u1w5ye.execute-api.us-east-1.amazonaws.com/testingstage";

  lambda(x:number, y:number):Observable<any>{
    let json = {
      "data": 
        {
          "x": x,
          "y": y
        }
    }
    let body  = JSON.stringify(json);
    console.log("testing");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private http:HttpClient) { }
}

