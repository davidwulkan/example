import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  
  
  city:string;
  temperature:number; 
  image:string; 
  hasError:boolean = false;
  errorMessage:string;
  weatherData$:Observable<Weather>; //מסוג OBSERVALBLE - מושך נתונים מסוג WEATHER
  lon:number; 
  lat:number;
  description:string;
  country:string; 
  
  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { }

  likes:number = 0;

  addLikes(){
    this.likes ++ //likes that belongs to this section...
  }


  ngOnInit(): void {
    this.city = this.route.snapshot.params.city; 
    this.weatherData$ = this.weatherService.searchWeatherData(this.city); 
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lon = data.lon;
        this.lat = data.lat;  
        this.description = data.description;
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )
  }
}

