// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDWcV9WOypnn6uKTVLidM9rh-k3hpb4mAU",
    authDomain: "example-e3131.firebaseapp.com",
    projectId: "example-e3131",
    storageBucket: "example-e3131.appspot.com",
    messagingSenderId: "411878903203",
    appId: "1:411878903203:web:e6ef37bd44914c8e8bde16"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
